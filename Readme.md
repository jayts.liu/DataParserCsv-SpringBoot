# DataParserCsv

 This prototype demonstrate of using frameworks to import data from csv into database. The system provides flexibility for user to specify the source from and target to by setting files.

### Prerequisites

 The project was tested under windows environment.

 1. Java 8+
 2. Spring-boot 2
 3. apache.commons 4.1
 4. apache.commons csv 1.5
 5. mssql-jdbc 6.3.6
 6. [JUtil-0.1.jar](http://gitlab.com/jayts.liu/JUtil)


### Installing

 1. Ensure your project configured and able to query with database.
 2. Place BasePersistence to your project
 3. Install spring-boot、apache.commons、 poi、mssql-jdbc libs into your project lib. 
 ```
    # configure build.gradle, add following into dependencies
    
	compile group: 'com.microsoft.sqlserver', name: 'mssql-jdbc', version: '6.3.6.jre8-preview'
	compile group: 'org.apache.commons', name: 'commons-csv', version: '1.5'
	compile group: 'org.apache.commons', name: 'commons-collections4', version: '4.1'

	compile('org.springframework.boot:spring-boot-starter-jdbc')
 ```

 4. Add JUtil-0.1.jar into your lib see.[JUtil-0.1.jar](http://gitlab.com/jayts.liu/JUtil)
 5. configure dataParserCsv.properties
 ```
    dataParserCsv.import.template.path=template/ #set your template path
    dataParserCsv.update.min.row=5000            #set minimum commit row per time 
    dataParserCsv.report.path=doc/report/        #set your data source path(csv files)
 ```
 
 6. Configure database file src/main/resources/application.properties
 ```
 	spring.datasource.url=jdbc:sqlserver://yourDatabaseIp:1433;databaseName=yourDatabaseName
	spring.datasource.username=yourDatabaseLoginName
	spring.datasource.password=yourDatabaseLoginPassword
	spring.datasource.driverClassName=com.microsoft.sqlserver.jdbc.SQLServerDriver
	...
 	
 ```
 
 7. Setup template file for system to pair with source file and db target table.
 ```
    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <impCfgTask>
     <source>
		<sourceRootPath>import csv path</sourceRootPath>
		<fileName>source file name(Optional)</fileName>
		<skipTitleLength>number to skip title length</skipTitleLength>
		<sourceColumnLength>source column length</sourceColumnLength>
		<sourceTable>temp table name(Optional)</sourceTable>
		<sourceBackupPath>set backup file path(Optional)</sourceBackupPath>
		<removeSourceFile>true/false to remove source file after backup(Optional)</removeSourceFile>
     </source>
     <destination>
		<destinationTable>target table name</destinationTable>
		 <reflect>
			<sequence>sequence data order(begin from 1)</sequence>
			<sourceColumn>from source column name</sourceColumn>
			<destinationColumn>to target column name</destinationColumn>
		 </reflect>
     </destination>
    </impCfgTask>
 ```
 8. now system is ready to run dataParserCsv/DataParserCsvSpringBootApplication.java
 9. check report file under doc/report
 10. Finish.


### Example

 template/sample.xml
 ~~~bash
  <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
  <configTask>
	<source>
		<sourceRootPath>doc/</sourceRootPath>           #system task will scan setup path to check csv files 
		<fileName>importSample.csv</fileName>          #allow empty, if set system will only import specified file
		<skipTitleLength>1</skipTitleLength>			  #if there are header please set number rows to skip
		<sourceColumnLength>5</sourceColumnLength>		  #set for system to number of column per row to insert
		<sourceTable>TEMPTABLE</sourceTable>            #if wish to store to template table before store to target table
		<sourceBackupPath>doc/backup</sourceBackupPath> #path to back up source file
		<removeSourceFile>false</removeSourceFile>      #remove source file after insert complete
	</source>
	<destination>
		<destinationTable>CUSTOMERSTEST</destinationTable> #insert into table db.CUSTOMERSTEST
		<reflect>
			<sequence>1</sequence>                          #tag sequence in csv file
			<sourceColumn>ID</sourceColumn>                 #csv tag name
			<destinationColumn>ID</destinationColumn>       #store to database column name
		</reflect>
		<reflect>
			<sequence>2</sequence>
			<sourceColumn>CUS_CODE</sourceColumn>
			<destinationColumn>CUS_CODE</destinationColumn>
		</reflect>
		<reflect>
			<sequence>3</sequence>
			<sourceColumn>CUS_NAME</sourceColumn>
			<destinationColumn>CUS_NAME</destinationColumn>
		</reflect>
		<reflect>
			<sequence>4</sequence>
			<sourceColumn>HADDRESS</sourceColumn>
			<destinationColumn>HADDRESS</destinationColumn>
		</reflect>
		<reflect>
			<sequence>5</sequence>
			<sourceColumn>PRICE</sourceColumn>
			<destinationColumn>PRICE</destinationColumn>
		</reflect>
	</destination>
  </configTask>
 ~~~

 doc/importSample.csv
 ~~~bash
  ID	CUS_CODE	CUS_NAME	ADDRESS	PRICE
  1	cus1	aname1	addressaddress1	1111.2
  2	cus2	aname2	addressaddress2	22.22
  3	cus3Update2223			33.32
 ~~~

### Run by jar file
 1. Under command line go to project root C:\DataImporterTest.
 2. Run command "gradlew build" to build jar file. in this case file locate at c:\DataParserCsv-SpringBoot\build\libs\DataParserCsv-SpringBoot-0.0.1-SNAPSHOT.jar.
 3. Create 3 folders '/doc', '/res', and '/template' where DataParserCsv.jar file located.
 4. Make sure csv source files are in '/doc' folder, Ex. '/doc/importSample.csc'
 5. Make sure dataParserCsv.properties is in '/res' folder. Ex. '/res/dataParserCsv.properties'
 6. Make sure xml configuration files are in '/template' folder. Ex. '/template/sample.xml'
 7. Run command "java -jar DataParserCsv-SpringBoot-0.0.1-SNAPSHOT.jar" to start import data.

### Build With

* [Spring Framework](https://spring.io) - The framework used
* [GRADLE](https://gradle.org/) - Dependency Management
* [Apache common](https://commons.apache.org/) - The framework used
* [mssql-jdbc] - The framework used

### License

 This project is licensed under the Apache License, Version 2.0 License - see the [LICENSE](LICENSE) file for details


