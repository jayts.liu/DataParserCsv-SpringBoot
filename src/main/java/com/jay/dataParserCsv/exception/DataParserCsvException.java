/*
 * Copyright 2018 Jay Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jay.dataParserCsv.exception;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @since 20180518
 * @version 0.1
 */
public class DataParserCsvException extends Exception {

	public DataParserCsvException() {
		super();
	}
	
	public DataParserCsvException(String message) {
		super(message);
	}
	
	public DataParserCsvException(Throwable t) {
		super(t);
	}
	
	public DataParserCsvException(String message, Throwable t) {
		super(message, t);
	}
	
}
