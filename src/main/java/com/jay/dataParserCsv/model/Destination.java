/*
 * Copyright 2018 Jay Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jay.dataParserCsv.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @since 20180518
 * @version 0.1
 */
@XmlAccessorType(XmlAccessType.NONE)
public class Destination {
	
	private String destinationTable;// <destinationTable>target table name</destinationTable>
	private List<Reflect> reflectList; // <reflect>
	
	private int procee;
	private String[] sourceColumn;
	private String[] destinationColumn;
	
	@XmlElement(nillable=false)
	public String getDestinationTable() {
		return destinationTable;
	}

	public void setDestinationTable(String destinationTable) {
		this.destinationTable = destinationTable;
	}

	@XmlElement(name = "reflect")
	public List<Reflect> getReflectList() {
		return reflectList;
	}

	public void setReflectList(List<Reflect> reflectList) {
		this.reflectList = reflectList;
	}
	
	public int getProcee() {
		return procee;
	}

	public void setProcee(int procee) {
		this.procee = procee;
	}
	
	public String[] getSourceColumn() {
		if(sourceColumn == null)
			sourceColumn = new String[reflectList.size()];
			for (int i = 0; i < reflectList.size(); i++) 
				sourceColumn[i] = reflectList.get(i).getSourceColumn();
			
		return sourceColumn;
	}
	
	public String[] getDestinationColumn() {
		if(destinationColumn == null)
			destinationColumn = new String[reflectList.size()];
			for (int i = 0; i < reflectList.size(); i++) 
				destinationColumn[i] = reflectList.get(i).getDestinationColumn();
			
		return destinationColumn;
	}
	
	public String[] getColumnList(int flowType) {
		if(flowType==1) 
			return getSourceColumn();
		return getDestinationColumn();
	}
}
