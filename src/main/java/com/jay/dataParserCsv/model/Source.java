/*
 * Copyright 2018 Jay Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jay.dataParserCsv.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @since 20180518
 * @version 0.1
 */
public class Source {
	
	private String sourceRootPath;
	private String fileName;// <fileName>source file name</fileName>
	private Integer skipTitleLength;// <skipTitleLength>skip titile length</skipTitleLength>
	private Integer sourceColumnLength;// <sourceColumnLength>source column length</sourceColumnLength>
	private String sourceTable;// <sourceTable>temp table name</sourceTable>
	private String sourceBackupPath;
	private boolean removeSourceFile; //set true to delete source file

	@XmlElement(nillable=false)
	public String getSourceRootPath() {
		return sourceRootPath;
	}

	public void setSourceRootPath(String sourceRootPath) {
		this.sourceRootPath = sourceRootPath;
	}

	@XmlElement(nillable=true)
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@XmlElement(nillable=true)
	public Integer getSkipTitleLength() {
		return skipTitleLength;
	}

	public void setSkipTitleLength(Integer skipTitleLength) {
		this.skipTitleLength = skipTitleLength;
	}

	@XmlElement(nillable=false)
	public Integer getSourceColumnLength() {
		return sourceColumnLength;
	}

	public void setSourceColumnLength(Integer sourceColumnLength) {
		this.sourceColumnLength = sourceColumnLength;
	}

	@XmlElement(nillable=true)
	public String getSourceTable() {
		return sourceTable;
	}

	public void setSourceTable(String sourceTable) {
		this.sourceTable = sourceTable;
	}

	@XmlElement(nillable=true)
	public String getSourceBackupPath() {
		return sourceBackupPath;
	}

	public void setSourceBackupPath(String sourceBackupPath) {
		this.sourceBackupPath = sourceBackupPath;
	}

	@XmlElement(nillable=true)
	public boolean isRemoveSourceFile() {
		return removeSourceFile;
	}

	public void setRemoveSourceFile(boolean removeSourceFile) {
		this.removeSourceFile = removeSourceFile;
	}

}