/*
 * Copyright 2018 Jay Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jay.dataParserCsv.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @since 20180518
 * @version 0.1
 */
public class Reflect {

	private int sequence;
	private String sourceColumn;// <souceColumn>from source column name</souceColumn>
	private String destinationColumn;// <destinationColumn>to target column name</destinationColumn>
	
	@XmlElement(nillable=false)
	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	@XmlElement(nillable=false)
	public String getSourceColumn() {
		return sourceColumn;
	}

	public void setSourceColumn(String sourceColumn) {
		this.sourceColumn = sourceColumn;
	}

	@XmlElement(nillable=false)
	public String getDestinationColumn() {
		return destinationColumn;
	}

	public void setDestinationColumn(String destinationColumn) {
		this.destinationColumn = destinationColumn;
	}

}