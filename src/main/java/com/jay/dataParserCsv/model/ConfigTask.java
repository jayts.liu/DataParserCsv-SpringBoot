/*
 * Copyright 2018 Jay Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jay.dataParserCsv.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @since 20180518
 * @version 0.1
 */
@XmlRootElement
public class ConfigTask {

	private String xmlName;
	private Source source;
	private List<Destination> destinationList;

	public ConfigTask() {
		super();
	}
	
	public Source getSource() {
		return source;
	}

	public void setSource(Source source) {
		this.source = source;
	}
	
	@XmlElement(name = "destination")
	public List<Destination> getDestinationList() {
		return destinationList;
	}

	public void setDestinationList(List<Destination> destinationList) {
		this.destinationList = destinationList;
	}

	public String getXmlName() {
		return xmlName;
	}

	public void setXmlName(String xmlName) {
		this.xmlName = xmlName;
	}
	
	public String getDestinationTableToString() {
		StringBuilder sb = new StringBuilder();
		if (destinationList != null) 
			for(int i=0; i< destinationList.size(); i++) {
				if(i > 0) sb.append(",");
				sb.append(destinationList.get(i).getDestinationTable());
			}
		return sb.toString();
	}

	@Override
	public String toString() {
		return "ImpCfgTask [source=" + source + ", destination=" + getDestinationTableToString() + "]";
	}

}




