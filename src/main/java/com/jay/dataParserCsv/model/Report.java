/*
 * Copyright 2018 Jay Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jay.dataParserCsv.model;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

import com.jay.base.util.common.PropertiesLoder;

/**
 * To show task report
 * @author Jay (JayTs.Liu@gmail.com)
 * @since 20180518
 * @version 0.1
 */
public class Report {

	private final static String REPORT_PATH = PropertiesLoder.getText("res/dataParserCsv.properties", "dataParserCsv.report.path");

	ResourceBundle bundle = ResourceBundle.getBundle(Report.class.getName());
	
	private String templatePath;
	private int updateMinRow;
	private int statusCountS;
	private int statusCountO;
	private int totRowCount;
	private int totInsertRowCount;
	private int totErrCount;
	private List<String> errMsgList;
	private List<ReportTask> reportTaskList;

	public Report() {}
	
	public Report(String templatePath, int updateMinRow) {
		this.templatePath = templatePath;
		this.updateMinRow = updateMinRow;
	}

	public List<String> getErrMsgList() {
		if (this.errMsgList == null)
			this.errMsgList = new ArrayList<>();
		return this.errMsgList;
	}

	public void setErrMsgList(List<String> errMsgList) {
		this.errMsgList = errMsgList;
	}

	public void addErrMsg(String message) {
		getErrMsgList().add(message);
	}

	public void addErrMsg(Throwable t) {
		addErrMsg("Error：" + t.getMessage());
	}

	public void addErrMsg(String message, Throwable t) {
		addErrMsg(message + " Error：" + t.getMessage());
	}

	public List<ReportTask> getReportTaskList() {
		if(this.reportTaskList == null)
			this.reportTaskList = new ArrayList<>();
		return this.reportTaskList;
	}

	public void setReportTaskList(List<ReportTask> reportTaskList) {
		this.reportTaskList = reportTaskList;
	}
	
	public void addReportTask(ReportTask reportTask) {
		getReportTaskList().add(reportTask);
	}

	public String getTemplatePath() {
		return templatePath;
	}

	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}

	public int getUpdateMinRow() {
		return updateMinRow;
	}

	public void setUpdateMinRow(int updateMinRow) {
		this.updateMinRow = updateMinRow;
	}

	public String toReport() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		sb.append(" [Memory Total:" + Runtime.getRuntime().totalMemory() );
    	sb.append(" Free:" + Runtime.getRuntime().freeMemory());
    	sb.append(" Max" + Runtime.getRuntime().maxMemory() + "]\n");
    	sb.append("--------------------------------------------------------\n");
    	sb.append("Template path：" + templatePath + "\n");
    	sb.append("minimum update record/time：" + updateMinRow + "\n");
    	
    	
		int x=0;
		for(ReportTask rd : getReportTaskList()) {
			sb.append("--------------------------------------------------------\n");
			sb.append((x+=1) + ".\n");
			sb.append(getText("text.configName")).append("： " + rd.getConfigName() + "\n"); //configure file(xml)
			sb.append(getText("text.sourceFile")).append("： " + rd.getSourceFile()+ "\n"); //source file row count
			
			if(!rd.getBackUpPath().equals("")) {
				sb.append(getText("text.backupPath")).append("：" + rd.getBackUpPath() +"\n"); //backup path
				sb.append(getText("text.dataBackup")).append("：" + (rd.isBackSuccess() ? getText("text.backupSuccess.true") : getText("text.backupSuccess.false")) + "\n"); //backup result
			}
			
			totRowCount += rd.getRowCount();
			totInsertRowCount += rd.getInsertRowCount();
			sb.append(getText("text.import.execl.rowCount")).append("： " + rd.getRowCount() + "\n");
			sb.append(getText("text.import.database.rowCount")).append("： " + rd.getInsertRowCount() + "\n");
			sb.append(getText("text.tempTable")).append("： " + rd.getTemptable() + "\n"); // rowCount iscomplete
			sb.append(getText("text.destineTable")).append("： " + rd.getTargetTables() + "\n");// rowCount iscomplete
			
			sb.append(getText("text.processStatus")).append("： " + rd.getStatus() + "\n");
			if(rd.getStatus().equals("S"))
				statusCountS += 1;
			else
				statusCountO += 1;
				
			totErrCount += rd.getErrMsgList().size();//total err
			sb.append(getText("text.errorMessage")).append(" ： " + rd.getErrMsgList().size() + "\n");
			for (int i = 0; i < rd.getErrMsgList().size(); i++)
				sb.append(i + 1 + ") " + rd.getErrMsgList().get(i) + "\n");
		}
		
		sb.append("--------------------------------------------------------\n");
		totErrCount = (totErrCount == 0 ? getErrMsgList().size() : totErrCount);
		sb.append(getText("text.otherErrorMessage")).append("：").append(getErrMsgList().size()).append(" \n");
		for (int i = 0; i < getErrMsgList().size(); i++)
			sb.append(i + 1).append(") " + getErrMsgList().get(i) + "\n");
		
		sb.append("\n=========================").append(getText("text.overall")).append("===========================\n");
		sb.append(" JOB： " + getReportTaskList().size());
		sb.append("  ").append(getText("text.task")).append("： ").append(statusCountS);
		sb.append("  ").append(getText("text.other")).append("：" + statusCountO);
		sb.append("  ").append(getText("text.import.excel")).append("： " + totRowCount);
		sb.append("  ").append(getText("text.import.database")).append("： " + totInsertRowCount + "\n ");
		sb.append("  ").append(getText("text.error")).append("： " + totErrCount + "\n");
		
		return sb.toString();
	}
	
	/**
	 * display report & export to file
	 * @throws IOException
	 */
	public void print() throws IOException {
		String rpt = toReport();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

		Path path = FileSystems.getDefault().getPath(REPORT_PATH);
		if(Files.notExists(path)) //check path exist
			Files.createDirectories(path);
		
		String fileName = sdf.format(new Date()) + String.format("%03d", (new Random().nextInt(1000) + 1));
		Path file = Paths.get(REPORT_PATH + fileName + ".txt");
		Files.write(file, rpt.getBytes());
		
		System.out.println(rpt);//display in console
	}
	
	private String getText(String key) {
		try {
			return bundle.getString(key);
		} catch (Exception e) {
			return key;
		}
	}

}

