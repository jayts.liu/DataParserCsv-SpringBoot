/*
 * Copyright 2018 Jay Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jay.dataParserCsv;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import com.jay.base.util.common.FileUtil;
import com.jay.base.util.common.FolderUtil;
import com.jay.base.util.common.PropertiesLoder;
import com.jay.dataParserCsv.exception.DataParserCsvException;
import com.jay.dataParserCsv.model.ConfigTask;
import com.jay.dataParserCsv.model.Destination;
import com.jay.dataParserCsv.model.Report;
import com.jay.dataParserCsv.model.ReportTask;
import com.jay.dataParserCsv.model.Source;
import com.jay.dataParserCsv.service.ServiceBroker;

/**
 * import csv data into database table by reading XML setting
 * @author Jay (JayTs.Liu@gmail.com)
 * @since 20180518
 * @version 0.1
 */
@Controller
public class DataParserCsv {
	
	private final static Properties CONFIG = PropertiesLoder.load("res/dataParserCsv.properties");
	private final static String TEMP_PATH = CONFIG.getProperty("dataParserCsv.import.template.path");
	private final static Integer UPDATE_MIN_ROW = Integer.valueOf(CONFIG.getProperty("dataParserCsv.update.min.row")); //update row per time

	@Autowired
	private ServiceBroker serviceBroker;

	private Report report;

	public void exec() throws IOException {
		initPathInfo();
		List<ConfigTask> configTaskList = parseToConfigTask(getXmlConfigList()); //prepare configure task
		processConfigTask(configTaskList); //start analyze and data insert
		getReport().print(); //print process result
	}
	
	public Report getReport() {
		if(report == null)
			this.report = new Report(TEMP_PATH, UPDATE_MIN_ROW);
		return this.report;
	}

	private void processConfigTask(List<ConfigTask> configTaskList) {
		
		for(ConfigTask task : configTaskList) {
				
				String rootPath = task.getSource().getSourceRootPath();
				String sourceFileName = task.getSource().getFileName();
				List<String> sourceFileList = null;
				
				//validate
				if(rootPath == null || rootPath.equals("")) {
					addErrMsg(task.getXmlName()+".sourceRootPath can not be empty! ");
					continue;
				}
				
				//prepare soure file list
				if(sourceFileName != null && !sourceFileName.isEmpty()) {
					sourceFileList = new ArrayList<>();
					sourceFileList.add(rootPath + sourceFileName);
				}
				else {
					sourceFileList = getSourceFileList(rootPath);
				}
				
				//start parsing
				for(String srcFile: sourceFileList) {
					try {
						processSourceData(task, srcFile);
					} catch (Exception e) {
						e.printStackTrace();
						addErrMsg("Config:" + task.getXmlName() + " Source:" +srcFile + " Error:" + e.getMessage());
					} 
				}

		}//end for
	}
	
	@Transactional
	private boolean processSourceData(ConfigTask task, String sourceFile) throws DataParserCsvException {
		System.out.println("[Start task: "+task.getXmlName() + "]");
		Source source = task.getSource();
		List<Destination> dstLst = task.getDestinationList();
		ReportTask reportTask = new ReportTask(task, sourceFile);
		
		Reader reader=null;
		try {
			reportTask.setStatus("I"); //I: Import
			//initial source file
	        reader = new FileReader(sourceFile);
			Iterable<CSVRecord> records = CSVFormat.EXCEL.withDelimiter('|').parse(reader);
	        
	        int skipRow = source.getSkipTitleLength();
            int flowType = (source.getSourceTable().equals("") ? 2 : 1); //1:store to temp table, 2:store to destination
	        int rowCnt=0;//row
	        int trunkCnt = 0;
	        List<String> sqlList = new ArrayList<>();
	        
	        /* process each row */
	        reportTask.setStatus("R");//R:Rows
	        for(CSVRecord row : records) {
	        	if(skipRow > 0 && rowCnt < skipRow) { //to skip rows
	        		rowCnt++;
	        		continue;
	        	}
	        
	            List<List<Object>> dataList = new ArrayList<>();
	            List<Object> objLst = new ArrayList<>();
	            int col=0;//column

	            /* process each cell */ 
	            for(int cellIdx=0; cellIdx < row.size(); cellIdx++) {
	            	String currentCell = row.get(cellIdx);
	            	
	            	/* process each destination (reflect mapping) */
	                for(int dlIdx=0; dlIdx < dstLst.size(); dlIdx++) { 
	                	if(dataList.isEmpty() || dataList.size() < dstLst.size()) {
	                		objLst = new ArrayList<Object>();
	                		dataList.add(dlIdx, objLst);
	                	}
	                	else { 
	                		objLst = dataList.get(dlIdx);
	                	}
	                	
	                	Destination d = dstLst.get(dlIdx);
	                	int procee = d.getProcee();
	                	int seq = d.getReflectList().get(procee).getSequence()-1; //start from 0
		                if(col==seq) {
		                	//add parameter
		                	objLst.add(currentCell);
		                	
			                dataList.set(dlIdx, objLst);
			                d.setProcee((procee+1 < d.getReflectList().size()) ? ++procee : 0);
			                dstLst.set(dlIdx, d);
		                }
	                }
	                col++;
	            }

	            reportTask.setStatus("Q");//Q:Prepare Query
	            //prepare insert query
	            for(int i=0; i < dstLst.size();i++) {
                	Destination d = dstLst.get(i);
                	String targetTabel = (flowType ==1? source.getSourceTable() : d.getDestinationTable()); 
                	
                	if(d.getColumnList(flowType).length > dataList.get(i).size())//check column length
                		throw new DataParserCsvException("unexpected column length error (expected:" + d.getColumnList(flowType).length + " , source:" + dataList.get(i).size() +")");
 
                	sqlList.add(getSql(targetTabel, d.getColumnList(flowType), dataList.get(i)));
                	trunkCnt++;
	            }
	            
	            if(trunkCnt >= UPDATE_MIN_ROW) {//check limit exceed 
	            	reportTask.setStatus("DB");//Database
	            	execQuery(sqlList);// do insert
	            	
	            	reportTask.addInsertRowCount(trunkCnt);
	            	trunkCnt=0;
	            }
	            
	            rowCnt++;
	            reportTask.addRowCount();
	        }//end while iterator
	        
	        if(sqlList != null ) { //flush data in trunk
	        	reportTask.setStatus("FDB");//Database
	        	execQuery(sqlList);
	        	reportTask.addInsertRowCount(trunkCnt);
	        	trunkCnt=0;
	        }
	        
	        if(flowType==1) { //run query to insert data form temp table to target table
	        	reportTask.setStatus("T");
	        	doInsertFromTempTable(dstLst, source.getSourceTable());
	        }
	        
	        if(!source.getSourceBackupPath().equals("")) { //backup source file
	        	reportTask.setStatus("B");
	        	doBackupSourceFile(source);
	        	reportTask.setBackSuccess(true);
	        }
	        
	        reportTask.setStatus("S");
	        System.out.println("[End task: "+task.getXmlName() + "]");
	        return true;
		}  catch (Exception e) {
			reportTask.addErrMsg(e.getMessage() + " row#" + (reportTask.getRowCount()+1));
			throw new DataParserCsvException(e);
		} finally {
			getReport().addReportTask(reportTask); //add task rport
			try { if(reader !=null) reader.close(); } catch (IOException e) {e.printStackTrace();}
		} 
	}
	
	private void doInsertFromTempTable(List<Destination> destinationList, String sourceTable) {
		List<String> sqlList = new ArrayList<>();
		for(Destination d: destinationList)
			sqlList.add(getSqlTempToTarget(sourceTable, d));
			
		execQuery(sqlList);
	}

	private void doBackupSourceFile(Source source) {
		List<String> fileList = new ArrayList<>();
		String sourceFile = source.getSourceRootPath()+ source.getFileName();
		String targetFile = source.getSourceBackupPath();
		boolean removeYn = source.isRemoveSourceFile();
		
		File sFile = new File(sourceFile); 
		if(sFile.isDirectory())
			fileList.addAll(FolderUtil.getTreeFiles(sourceFile, "csv"));
			//fileList.addAll(FolderUtil.getTreeFiles(sourceFile, "xlsx", "xls"));
		else
			fileList.add(sFile.getPath());
		
		for(String sf : fileList) {
			try {
				FileUtil.copyFile(sf, targetFile+ sf.substring(sf.lastIndexOf("\\")), removeYn);
			} catch (IOException e) {
				e.printStackTrace();
				addErrMsg(e.getMessage());
			}
		}//end for
	}
	
	
	private String getSql(String tableName, String[] columnList, List<Object> dataList) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ");
		sql.append(tableName);
		sql.append("(");
		sql.append(Arrays.toString(columnList).replaceAll("[\\[\\]]", ""));
		sql.append(") ");
		sql.append("VALUES(");
		sql.append(getParmasToSqlStr(dataList));
		sql.append("); ");
		
		return sql.toString();
	}
	
	private String getSqlTempToTarget(String sourceTable, Destination destination) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ");
		sql.append(destination.getDestinationTable());
		sql.append("(");
		sql.append(Arrays.toString(destination.getSourceColumn()).replaceAll("[\\[\\]]", ""));
		sql.append(") ");
		sql.append("SELECT ");
		sql.append(Arrays.toString(destination.getDestinationColumn()).replaceAll("[\\[\\]]", ""));
		sql.append(" FROM ").append(sourceTable).append("; ");

		return sql.toString();
	}
	
	private List<String> getXmlConfigList() {
		return FolderUtil.getTreeFiles(TEMP_PATH, ".xml", ".Xml", ".XML");
	}

	private List<String> getSourceFileList(String path) {
		//return FolderUtil.getTreeFiles(TEMP_PATH, ".xlsx");
		return FolderUtil.getTreeFiles(TEMP_PATH, ".csv");
	}
	
	private String getParmasToSqlStr(List<Object> dataList) {
		StringBuilder sb = new StringBuilder();
		for(int i=0; i < dataList.size() ; i++) {
			if(i > 0)
				sb.append(", ");
			
			if(dataList.get(i) == null)
				sb.append("NULL");
			else if(dataList.get(i) instanceof Number)
				sb.append(dataList.get(i));
			else
				sb.append("'").append(String.valueOf(dataList.get(i)).replaceAll("'", "")).append("'");
		}
		return sb.toString();
	}
	
	private int execQuery(List<String> sqlList) {
		return serviceBroker.execQuery(sqlList).length; 
	}
	
	private List<ConfigTask> parseToConfigTask(List<String> xmlList) { //initial configure file
		List<ConfigTask> accurateTaskList = null;
		
		if (xmlList != null && xmlList.size() > 0) {
			accurateTaskList = new ArrayList<ConfigTask>();
			for (String fileStr : xmlList) {
				File file = new File(fileStr);
				JAXBContext jaxbContext;
				try {
					jaxbContext = JAXBContext.newInstance(ConfigTask.class);
				
					Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
					ConfigTask ct = (ConfigTask) jaxbUnmarshaller.unmarshal(file);
					ct.setXmlName(file.getName());
					
					accurateTaskList.add(ct);
				} catch (JAXBException e) {
					Throwable t = (e.getMessage() == null ? e.getLinkedException() : e);
					getReport().addErrMsg(fileStr, t);
					e.printStackTrace();
				}
			}
		}
		
		return accurateTaskList;
	}
	
	private void addErrMsg(String message) {
		getReport().addErrMsg(message);
	}
	
	private void initPathInfo() {
		System.out.println("CONFIG: " + CONFIG);
		System.out.println("TEMP_PATH: " + TEMP_PATH);
		System.out.println("UPDATE_MIN_ROW: " + UPDATE_MIN_ROW);
	}
}

